from src.transformacion.transformacion import Transformacion
from datetime import datetime

class mainTransf():
    
    def __init__(self,dataframe) -> None:
        """
        Method: Constructor de la clase que inicializa la orquestacion de la transformacion de los datos
        
        """
        self.dic_transformacion = {}
        self.dic_transformacion["proceso"] = "transformacion"
        fecha_inicio = datetime.now()
        self.dic_transformacion["hora_inicio"] = fecha_inicio.strftime("%Y-%m-%d %H:%M:%S")
        self.dic_transformacion["cantidad_nulos"] = 0
        self.dic_transformacion["cantidad_registros"] = 0
        self.dic_transformacion["cantidad_columnas"] = 0
        try:
            segunda_ejecuccion = Transformacion(dataframe,self.dic_transformacion)
            self.dataframe,self.dic_transformacion = segunda_ejecuccion.ejecuccion_transformacion()
            self.dic_transformacion["estado"] = "exito"
            self.dic_transformacion["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            
        except Exception as e:
            print(e)
            self.dic_transformacion["estado"] = "fallido"
            self.dic_transformacion["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print(self.dic_transformacion)
        
        
    def get_dataframe(self):
        """
        Method: devuelve el dataframe de los datos transformados
        
        """
        return self.dataframe
    
    def get_dicc_transformacion(self):
        """
        Method: devuelve el diccionario de los datos transformados
        
        """
        return self.dic_transformacion