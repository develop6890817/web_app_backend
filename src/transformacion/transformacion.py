
import pandas as pd
import googlemaps
import country_converter as coco
import os
from dotenv import load_dotenv
from unidecode import unidecode
from datetime import datetime

class Transformacion():
    
    def __init__(self, dataframe,dic_transformacion) -> None:
        """
        Method: Constructor de la clase que inicializa el dataframe y las
        variables necesarias para la transformacion
        
        """
        self.pd = dataframe
        self.dic_transformacion = dic_transformacion
        
        
    def geo_referencia(self, nacionalidad, nombre_editorial=None):
        """
        Method: Realiza la construccion de la Geolocalizacion del la editorial y nacionalidad
        
        """
        gmaps = googlemaps.Client(key="AIzaSyAiN4Kc1PnxVPKanYLgDiwn8TA7A2psF3Y")

        if nombre_editorial !=None:
            direccion = f'book publishers {str(nombre_editorial)} , {str(nacionalidad)}'
        else:
            direccion = f'{str(nacionalidad)}'
        resultado = gmaps.geocode(direccion)
        latitud = resultado[0]['geometry']['location']['lat']
        longitud = resultado[0]['geometry']['location']['lng']
        territorio = resultado[0]["formatted_address"]
        return latitud,longitud,territorio
    
    def fecha(self, fecha_string):
        """
        Method: Realiza la construccion y division de la fecha para 
        la construccion de los tiempos
        
        """
        dias_semana = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
        fecha_datetime = datetime.strptime(fecha_string, '%Y-%m-%d')
        dia_semana = dias_semana[fecha_datetime.weekday()]
        trimestre = (fecha_datetime.month - 1) // 3 + 1
        semestre = (fecha_datetime.month - 1) // 6 + 1
        return dia_semana,trimestre,semestre,fecha_datetime.day,fecha_datetime.month,fecha_datetime.year
        
        
    def obtener_acronimo_pais(self,nombre_pais):
        """
        Method: Construye el acrononimo de los territorios
        
        """
        try:
            codigo_iso = coco.convert(names=nombre_pais, to='ISO2')
            return codigo_iso
        except:
            return "NAN"
        
    def eliminar_estandizar_columnas(self):
        """
        Method: Elimina la columna innecesaria y estandariza las columnas genero y territorio
        
        """
        columnas_eliminar = ['territorio_nacionalidad','territorio_editorial','autor_fecha_nacimiento']
        for item in columnas_eliminar:
            self.pd = self.pd.drop(columns=[item])

        columnas_estandarizar = ['autor_genero','ubicacion_editorial','autor_nacionalidad','dia_semana_nacimiento']
        for item in columnas_estandarizar:
            self.pd[item] = self.pd[item].apply(lambda x: unidecode(x).lower().replace(" ","_"))

    def extracion_dato_esquema(self):
        """
        Method: Realiza la construccion del dataframe y la carga a nivel
        de dato del esquema que actualmente se encuentra la informacion
        
        """
        nulos_por_columna = self.pd.isnull().sum().sum()
        self.dic_transformacion["cantidad_nulos"] = nulos_por_columna
        self.dic_transformacion["cantidad_registros"] = self.pd.shape[0]
        self.dic_transformacion["cantidad_columnas"] = self.pd.shape[1]
    
    
    def ejecuccion_transformacion(self):
        """
        Method: Orquesta los metodos encargados de la transformacion
        
        """
        self.pd[["dia_semana_nacimiento", "trimestre_nacimiento", "semestre_nacimiento","dia_nacimiento","mes_nacimiento","lapso_nacimiento"]] = self.pd["autor_fecha_nacimiento"].apply(self.fecha).apply(pd.Series)
        self.pd[['latitud_editorial', 'longitud_editorial','territorio_editorial']] = self.pd.apply(lambda x: self.geo_referencia(x['ubicacion_editorial'], x['nombre_editorial']), axis=1, result_type='expand')
        self.pd[['latitud_nacionalidad', 'longitud_nacionalidad','territorio_nacionalidad']] = self.pd.apply(lambda x: self.geo_referencia(x['autor_nacionalidad']), axis=1, result_type='expand')
        self.pd['acronimo_nacionalidad'] = self.pd['territorio_nacionalidad'].apply(lambda x: self.obtener_acronimo_pais(x))
        self.pd['acronimo_editorial'] = self.pd['territorio_editorial'].apply(lambda x: self.obtener_acronimo_pais(x))
        self.eliminar_estandizar_columnas()
        self.extracion_dato_esquema()
        return self.pd,self.dic_transformacion
       
