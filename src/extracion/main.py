from src.extracion.extracion import Extracion
from datetime import datetime

class mainExtr():
    
    def __init__(self) -> None:
        """
        Method: Constructor de la clase que inicializa la orquestacion de la extracion de los datos
        
        """
        self.dic_extracion = {}
        self.dic_extracion["proceso"] = "extracion"
        fecha_inicio = datetime.now()
        self.dic_extracion["hora_inicio"] = fecha_inicio.strftime("%Y-%m-%d %H:%M:%S")
        self.dic_extracion["cantidad_nulos"] = 0
        self.dic_extracion["cantidad_registros"] = 0
        self.dic_extracion["cantidad_columnas"] = 0
        try:
            primera_ejecuccion = Extracion(self.dic_extracion)
            self.dataframe,self.dic_extracion = primera_ejecuccion.ejecuccion_extracion()
            self.dic_extracion["estado"] = "exito"
            self.dic_extracion["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            
        except Exception as e:
            self.dic_extracion["estado"] = "fallido"
            self.dic_extracion["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print(self.dic_extracion)
    
    def get_dataframe(self):
        """
        Method: devuelve el dataframe de los datos extraidos
        
        """
        return self.dataframe
    
    def get_dicc_extracion(self):
        """
        Method: devuelve el diccionario de los datos extraidos
        
        """
        return self.dic_extracion