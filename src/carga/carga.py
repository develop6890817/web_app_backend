
import pandas as pd
class Carga():
    
    def __init__(self,dataframe,dic_carga) -> None:
        """
        Method: Constructor de la clase que inicializa el dataframe y las
        variables necesarias para la carga
        
        """
        self.pd_txt = dataframe
        self.dic_carga = dic_carga
       
    def tabla_maestra_autor(self):
        """
        Method: Realiza la construccion del dataframe de los 
        datos maestros que contendra los registros de los autores
        
        """
        mdm_autor = pd.DataFrame({
            'nombre': self.pd_txt["autor_nombre"],
            'fk_fecha_nacimiento': self.pd_txt['dia_nacimiento'].astype(str) + self.pd_txt['mes_nacimiento'].astype(str) + self.pd_txt['lapso_nacimiento'].astype(str),
            'fk_nacionalidad': self.pd_txt['autor_nacionalidad'].apply(lambda x: self.mdm_ubicacion.loc[self.mdm_ubicacion['territorio'] == x, 'id'].iloc[0])

        })
        mdm_autor = mdm_autor.drop_duplicates()
        mdm_autor = mdm_autor.reset_index()
        mdm_autor["id"] = mdm_autor["index"]
        mdm_autor = mdm_autor.drop(columns=["index"])
        columnas = ['id', 'nombre', 'fk_fecha_nacimiento','fk_nacionalidad']
        mdm_autor = mdm_autor[columnas]
        return mdm_autor
    
    def tabla_resumen_libro(self):
        """
        Method: Realiza la construccion del dataframe de los 
        datos resumen que contendra los registros de los libros
        
        """
        fact_libro = pd.DataFrame({
            'id': self.pd_txt["isbn"],
            'titulo': self.pd_txt["titulo"],
            'precio': self.pd_txt["precio"],
            'cantidad': self.pd_txt["cantidad_stock"],
            'fk_editorial':self.pd_txt['nombre_editorial'].apply(lambda x: self.mdm_edictorial.loc[self.mdm_edictorial['nombre_edictorial'] == x, 'id'].iloc[0]),
            'fk_autor':self.pd_txt['autor_nombre'].apply(lambda x: self.mdm_autor.loc[self.mdm_autor['nombre'] == x, 'id'].iloc[0]),
            'fk_genero_literario':self.pd_txt['autor_genero'].apply(lambda x: self.ctx_genero_literario.loc[self.ctx_genero_literario['nombre_genero'] == x, 'id'].iloc[0])
        })
        fact_libro = fact_libro.drop_duplicates()
        return fact_libro
        
    def tabla_maestra_editorial(self):
        """
        Method: Realiza la construccion del dataframe de los 
        datos maestros que contendra los registros de las editoriales
        
        """
        mdm_editorial = pd.DataFrame({
            'nombre_edictorial': self.pd_txt["nombre_editorial"],
            'fk_ubicacion_edictorial': self.pd_txt['ubicacion_editorial'].apply(lambda x: self.mdm_ubicacion.loc[self.mdm_ubicacion['territorio'] == x, 'id'].iloc[0])
        })
        mdm_editorial = mdm_editorial.drop_duplicates()
        mdm_editorial = mdm_editorial.reset_index()
        mdm_editorial["id"] = mdm_editorial["index"]
        mdm_editorial = mdm_editorial.drop(columns=["index"])
        columnas = ['id', 'nombre_edictorial', 'fk_ubicacion_edictorial']
        mdm_editorial = mdm_editorial[columnas]
        return mdm_editorial
    
    def tabla_maestra_tiempo(self):
        """
        Method: Realiza la construccion del dataframe de los 
        datos maestros que contendra los registros de las fechas
        
        """
        mdm_tiempo = pd.DataFrame({
            'id': self.pd_txt['dia_nacimiento'].astype(str) + self.pd_txt['mes_nacimiento'].astype(str) + self.pd_txt['lapso_nacimiento'].astype(str),
            'dia': self.pd_txt["dia_nacimiento"],
            'mes': self.pd_txt["mes_nacimiento"],
            'lapso': self.pd_txt["lapso_nacimiento"],
            'dia_semana': self.pd_txt["dia_semana_nacimiento"],
            'trimestre': self.pd_txt["trimestre_nacimiento"],
            'semestre': self.pd_txt["semestre_nacimiento"],

        })
        mdm_tiempo = mdm_tiempo.drop_duplicates()
        return mdm_tiempo

    def tabla_maestra_ubicacion(self,territorio,acronimo,latitud,longitud):
        """
        Method: Realiza la construccion del dataframe de los 
        datos maestros que contendra los registros de las ubicaciones
        
        """
        mdm_ubicacion = pd.DataFrame({
            'territorio': territorio,
            'acronimo': acronimo,
            'latitud': latitud,
            'longitud': longitud
        })
        mdm_ubicacion = mdm_ubicacion.drop_duplicates()
        return mdm_ubicacion
    
    def tabla_contexto_genero_literario(self):
        """
        Method: Realiza la construccion del dataframe de los 
        datos en contexto que contendra los registros de los generos literarios
        
        """
        ctx_genero_literario = pd.DataFrame({
            'nombre_genero': self.pd_txt["autor_genero"]
        })
        ctx_genero_literario = ctx_genero_literario.drop_duplicates()
        ctx_genero_literario = ctx_genero_literario.reset_index()
        ctx_genero_literario["id"] = ctx_genero_literario["index"]
        ctx_genero_literario = ctx_genero_literario.drop(columns=["index"])
        columnas = ['id', 'nombre_genero']
        ctx_genero_literario = ctx_genero_literario[columnas]
        return ctx_genero_literario

   
        
    def ajustes_tabla_ubicacion(self):
        """
        Method: Realiza la orquestacion de la creacion del dataframe de los 
        datos maestros de la ubicacion
        
        """
        mdm_ubicacion_1 = self.tabla_maestra_ubicacion(self.pd_txt.ubicacion_editorial,self.pd_txt.acronimo_editorial,self.pd_txt.latitud_editorial,self.pd_txt.longitud_editorial)
        mdm_ubicacion_2 = self.tabla_maestra_ubicacion(self.pd_txt.autor_nacionalidad,self.pd_txt.acronimo_nacionalidad,self.pd_txt.latitud_nacionalidad,self.pd_txt.longitud_nacionalidad)
        mdm_ubicacion = pd.concat([mdm_ubicacion_1, mdm_ubicacion_2])
        mdm_ubicacion = mdm_ubicacion.drop_duplicates()
        mdm_ubicacion = mdm_ubicacion.reset_index(drop=True)
        mdm_ubicacion = mdm_ubicacion.reset_index()
        mdm_ubicacion["id"] = mdm_ubicacion["index"]
        mdm_ubicacion = mdm_ubicacion.drop(columns=["index"])
        columnas = ['id', 'territorio', 'acronimo','latitud','longitud']
        mdm_ubicacion = mdm_ubicacion[columnas]
        return mdm_ubicacion
       
    def ejecuccion_carga(self):
        """
        Method: Orquesta los metodos encargados de la carga
        
        """
        self.mdm_tiempo = self.tabla_maestra_tiempo()
        self.mdm_ubicacion = self.ajustes_tabla_ubicacion()
        self.mdm_autor = self.tabla_maestra_autor()
        self.mdm_edictorial = self.tabla_maestra_editorial()
        self.ctx_genero_literario = self.tabla_contexto_genero_literario()
        self.fact_libro = self.tabla_resumen_libro()
        return self.mdm_tiempo,self.mdm_ubicacion,self.mdm_autor, self.mdm_edictorial, self.ctx_genero_literario, self.fact_libro 