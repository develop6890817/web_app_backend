from src.carga.carga import Carga
from datetime import datetime

class mainCarga():
    
    def __init__(self,dataframe) -> None:
        """
        Method: Constructor de la clase que inicializa la orquestacion de la carga de los datos
        
        """
        self.dic_carga = {}
        self.dic_carga["proceso"] = "carga"
        fecha_inicio = datetime.now()
        self.dic_carga["hora_inicio"] = fecha_inicio.strftime("%Y-%m-%d %H:%M:%S")
        self.dic_carga["cantidad_nulos"] = 0
        self.dic_carga["cantidad_registros"] = 0
        self.dic_carga["cantidad_columnas"] = 0
        try:
            tercera_ejecuccion = Carga(dataframe,self.dic_carga)
            self.mdm_tiempo,self.mdm_ubicacion,self.mdm_autor, self.mdm_editorial, self.ctx_genero_literario, self.fact_libro  = tercera_ejecuccion.ejecuccion_carga()
            self.dic_carga["cantidad_nulos"] = self.mdm_tiempo.isnull().sum().sum() + self.mdm_ubicacion.isnull().sum().sum() + self.mdm_autor.isnull().sum().sum() + self.mdm_editorial.isnull().sum().sum() + self.ctx_genero_literario.isnull().sum().sum() + self.fact_libro.isnull().sum().sum()
            self.dic_carga["cantidad_registros"] = self.mdm_tiempo.shape[0] + self.mdm_ubicacion.shape[0] + self.mdm_autor.shape[0] + self.mdm_editorial.shape[0] + self.ctx_genero_literario.shape[0] + self.fact_libro.shape[0]
            self.dic_carga["cantidad_columnas"] = self.mdm_tiempo.shape[1] + self.mdm_ubicacion.shape[1] + self.mdm_autor.shape[1] + self.mdm_editorial.shape[1] + self.ctx_genero_literario.shape[1] + self.fact_libro.shape[1]
            self.dic_carga["estado"] = "exito"
            self.dic_carga["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            
        except Exception as e:
            print(e)
            self.dic_carga["estado"] = "fallido"
            self.dic_carga["hora_finalizacion"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print(self.dic_carga)
        
        
    def get_mdm_tiempo(self):
        """
        Method: devuelve el dataframe de los datos maestros de los tiempos
        
        """
        return self.mdm_tiempo
    
    def get_mdm_ubicacion(self):
        """
        Method: devuelve el dataframe de los datos maestros de las ubicaciones
        
        """
        return self.mdm_ubicacion
    
    def get_mdm_autor(self):
        """
        Method: devuelve el dataframe de los datos maestros de los autores
        
        """
        return self.mdm_autor
    
    def get_mdm_editorial(self):
        """
        Method: devuelve el dataframe de los datos maestros de las editoriales
        
        """
        return self.mdm_editorial
    
    def get_ctx_genero_literario(self):
        """
        Method: devuelve el dataframe de los datos en contexto de los generos literarios
        
        """
        return self.ctx_genero_literario
    
    def get_fact_libro(self):
        """
        Method: devuelve el dataframe de los datos en resumen de los libros
        
        """
        return self.fact_libro 
    
    def get_dicc_carga(self):
        """
        Method: devuelve el diccionaro de los datos cargados
        
        """
        return self.dic_carga 