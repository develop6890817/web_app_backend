from flask import Flask, request, jsonify, render_template
from flask_restx import Api, Resource
from flask_swagger_ui import get_swaggerui_blueprint
import os
from dotenv import load_dotenv
import json
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity, unset_jwt_cookies, get_jwt
from orquestador import data
  
blacklist = set()
load_dotenv()
dicc_extracion,dicc_transformacion,carga = data()
SECRET_KEY = os.getenv('SECRET_KEY', 'valor_predeterminado')
user = os.getenv('USER_NAME', 'valor_predeterminado')
password = os.getenv('PASSWORD', 'valor_predeterminado')
app = Flask(__name__)
api = Api(app, version='1.0', title='API de Ejemplo', description='Una descripción de mi API')

app.config['JWT_SECRET_KEY'] = SECRET_KEY
jwt = JWTManager(app)
usuarios = { user: password}

@app.route('/login', methods=['POST'])
def login():
    datos = request.get_json()
    usuario = datos.get('user')
    contraseña = datos.get('password')

    if not usuario or not contraseña:
        return jsonify({"mensaje": "Falta el nombre de usuario o la contraseña"}), 400

    if usuario in usuarios and usuarios[usuario] == contraseña:
        token_de_acceso = create_access_token(identity=usuario, expires_delta=False)
        return jsonify({'token': token_de_acceso}), 200
    else:
        return jsonify({"mensaje": "Credenciales inválidas"}), 401

@app.route('/recurso-mdm_tiempo', methods=['GET'])
@jwt_required()  
def recurso_mdm_tiempo():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_mdm_tiempo().to_json(orient='records')), 200

@app.route('/recurso-mdm_autor', methods=['GET'])
@jwt_required()  
def recurso_mdm_autor():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_mdm_autor().to_json(orient='records')), 200

@app.route('/recurso-mdm_editorial', methods=['GET'])
@jwt_required()  
def recurso_mdm_editorial():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_mdm_editorial().to_json(orient='records')), 200

@app.route('/recurso-mdm_ubicacion', methods=['GET'])
@jwt_required()  
def recurso_mdm_ubicacion():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_mdm_ubicacion().to_json(orient='records')), 200

@app.route('/recurso-ctx_genero_literario', methods=['GET'])
@jwt_required()  
def recurso_ctx_genero_literario():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_ctx_genero_literario().to_json(orient='records')), 200

@app.route('/recurso-fact_libro', methods=['GET'])
@jwt_required()  
def recurso_fact_libro():
    usuario_actual = get_jwt_identity()
    return jsonify(carga.get_fact_libro().to_json(orient='records')), 200

@app.route('/recurso-dicc_ext', methods=['GET'])
@jwt_required()  
def recurso_dicc_ext():
    usuario_actual = get_jwt_identity()
    return jsonify(json.dumps(dicc_extracion)), 200

@app.route('/recurso-dicc_transf', methods=['GET'])
@jwt_required()  
def recurso_dicc_transf():
    usuario_actual = get_jwt_identity()
    return jsonify(json.dumps(dicc_transformacion)), 200

@app.route('/recurso-dicc_carga', methods=['GET'])
@jwt_required()  
def recurso_dicc_carga():
    usuario_actual = get_jwt_identity()
    return jsonify(json.dumps(carga.get_dicc_carga)), 200

@app.route('/logout', methods=['POST'])
@jwt_required()  
def logout():
    jwt_token = get_jwt()['jti']
    blacklist.add(jwt_token)
    response = jsonify({'mensaje': 'Cierre de sesión exitoso'})
    unset_jwt_cookies(response)
    
    return response, 200

SWAGGER_URL = '/api/docs'  # URL para acceder a la interfaz de Swagger
API_URL = '/static/swagger.json'  # URL del archivo JSON de la especificación de la API

# Configura la interfaz de Swagger
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # URL de la interfaz de Swagger
    API_URL,
    config={  # Configuración de la interfaz de Swagger
        'app_name': "API de Ejemplo"
    }
)

# Registra la interfaz de Swagger en tu aplicación Flask
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


if __name__ == '__main__':
    app.run(debug=True)