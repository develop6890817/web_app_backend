# web_app_frotend



## Inicio del proyecto

El desarrollo backend se realizo con la tecnologia de framework flasapi para realizar una entrega limpia y desarrollo etl con la herramienta pandas


## Integracion y correr el proyecto local se debe:

- [ ] [se debe crear el entorno virtual .env]
- [ ] [dentro del entorno local se instala las dependencias que contramos en el requeriments.txt, dentro de la carpeta /src]
- [ ] [al actualizar la ip y puerto del servicio backend, debe cambiarla en el entorno frotend]



## Funcionalidades

El proposito del apartado bakend es realizar una entrega y llevar control del proceso etl
- las rutas que se entrega, son de los datos procesados y  diccionario de los procesos
- para el proceso etl se maneja una maquina de estado cada vez que se cumpla un proceso
- para visualizar la documentacion es mediante la ruta: http://localhost:5000/api/docs/#/

![Ejemplo](https://gitlab.com/develop6890817/web_app_backend/-/raw/main/api.JPG?ref_type=heads)

## Authors and acknowledgment
Jose Manolo Pinzon Hernandez

## Inicio del proyecto
La contraseña para acceder a la api esta en el archivo de variable de entorno
