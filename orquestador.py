from src.extracion.main import mainExtr
from src.transformacion.main import mainTransf
from src.carga.main import mainCarga
import pandas as pd

def data():
    extracion = mainExtr()
    extracion_dataframe = extracion.get_dataframe()
    dicc_extracion = extracion.get_dicc_extracion()
    if dicc_extracion['estado']=='exito':
        transformacion = mainTransf(extracion_dataframe)
        transformacion_dataframe = transformacion.get_dataframe()
        dicc_transformacion = transformacion.get_dicc_transformacion()

    if dicc_transformacion['estado']=='exito':
        carga = mainCarga(transformacion_dataframe)
    return dicc_extracion,dicc_transformacion,carga
